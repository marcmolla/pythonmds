===============================================
pythonMDS: Python Molecular Dynamics Simulation
===============================================

Overview
========

Preliminary version of pythonMDS

Build
=====

For building the cython module (cython is required)::

    $ python setup.py build_ext --inplace

Introduction
============

The simulation API is in the simcore module. It offers a very basic method for running the simulation. In the same module you can found the core of the simulation, implemented in cython.

In the notebook directory you can find several Ipython notebooks with simulations. They are also available using nbviewew:
`Ideal Gas <http://nbviewer.ipython.org/urls/bitbucket.org/marcmolla/pythonmds/raw/e6841ca0399a8d77bf3234ae3f3b3211c9be1df2/notebooks/Gas%20ideal.ipynb>`_
`Free expansion <http://nbviewer.ipython.org/urls/bitbucket.org/marcmolla/pythonmds/raw/e6841ca0399a8d77bf3234ae3f3b3211c9be1df2/notebooks/Free_expansion.ipynb>`_
`Effusion <http://nbviewer.ipython.org/urls/bitbucket.org/marcmolla/pythonmds/raw/e6841ca0399a8d77bf3234ae3f3b3211c9be1df2/notebooks/Effusion.ipynb>`_
