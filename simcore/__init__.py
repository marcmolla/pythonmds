"""Core of the MD simulation

simcore module contains the classes required for the autonomous simulation

simcore.core_pure is the implementation of the simulation in Cython"""
from pythonmd.simcore.core_pure import MDSimulation
from configparser import ConfigParser

def compare_t_up(t_a, t_b):
    """Used for increasing T"""
    return t_a > t_b

def compare_t_down(t_a, t_b):
    """Used for decreasing T"""
    return t_a < t_b

class MDSimulationSession(object):
    """A session of MD simulation"""
    #Default number of molecules
    MOLECULES = 200
    #Default size
    SIZE = 60
    #FLOAT ERROR
    EPSILON = 1e-10
    #Time after T change
    RUN_T = 10
    #Time for stable step
    RUN_STABLE = 50
    #Delta stable T
    DELTA_T = 0.00001

    def __init__(self, simulation_filename=None):
        """Creates a MD simulation session. The simulation can be defined in
        the simulation_filename file"""
        self.motor = None
        if simulation_filename:
            self.load_simulation_file(simulation_filename)
        else:
            #Loads defaults
            self.motor = MDSimulation(self.MOLECULES, self.SIZE, self.SIZE)

    def load_simulation_file(self, simulation_filename):
        """Loads the simulation data from file"""
        config = ConfigParser()
        config.read(simulation_filename)

        wall_type = 0
        wall_hole = None
        if config.has_option("MD Simulation", 'wall'):
            wall_option = config.get("MD Simulation", "wall")
            if wall_option == "fixed":
                wall_type = 1
            elif wall_option == "effusion":
                wall_type = 2
                if config.has_option("MD Simulation", 'wall_hole'):
                    wall_hole = config.getint("MD Simulation", "wall_hole")
            elif wall_option == "floating":
                wall_type = 3
        wall_pos = None
        if config.has_option("MD Simulation", 'wall_pos'):
            wall_pos = config.getint("MD Simulation", 'wall_pos')

        self.motor = MDSimulation(config.getint("MD Simulation", "number_of_molecules"),
                                  config.getint("MD Simulation", "box_width"),
                                  config.getint("MD Simulation", "box_height"),
                                  wall_type, wall_pos, wall_hole)
        if config.has_section("GUI"):
            if config.get("GUI", "back-end") == "kivy":
                self.create_kivy_gui()

    def create_kivy_gui(self):
        """Creates a GUI based on kivy"""
        try:
            from md_simulation import MDApp
        except ImportError:
            raise Exception("Kivy App not found")
        MDApp(self.motor).run()

    def run(self, time):
        """Runs the simulation during *time* seconds."""
        if not self.motor:
            raise Exception("MD simulation is not configured")

        steps_per_second = int(1/self.motor.DELTA_T)
        start_time = self.motor.current_time

        delta_time = self.motor.current_time - start_time
        while abs(delta_time - time) > self.EPSILON and \
        time > 0 and \
        delta_time < time + 1:
            #Minimum step: 1 second
            for _ in range(steps_per_second):
                self.motor.step()
            delta_time = self.motor.current_time - start_time

    def get_molecules_speed(self):
        """Dumps the speed of the molecules"""
        speeds = []
        for n_mol in range(self.motor.number_of_molecules):
            speeds.append(self.motor.get_speed(n_mol))
        return speeds

    def get_stats(self):
        """Returns the simulation stats: current_time, T, p, E, K, U, V, n"""
        t_l, p_l, t_r, p_r = self.motor.get_stats()
        volume = self.motor.get_volumes()[0] + self.motor.get_volumes()[1]
        return self.motor.current_time, t_l + t_r, p_l + p_r, \
            self.motor.get_total_energy(), \
            self.motor.get_kinetic_energy(), \
            self.motor.get_potential_energy(), \
            volume, \
            self.motor.number_of_molecules

    def get_left_stats(self):
        """Returns the stats of left side of the box: current_time, T, p, V, n"""
        return self.motor.current_time, \
            self.motor.get_stats()[0], \
            self.motor.get_stats()[1], \
            self.motor.get_volumes()[0], \
            self.motor.get_number_molecules()[0]

    def get_right_stats(self):
        """Returns the stats of right side of the box: current_time, T, p, V, n"""
        return self.motor.current_time, \
            self.motor.get_stats()[2], \
            self.motor.get_stats()[3], \
            self.motor.get_volumes()[1], \
            self.motor.get_number_molecules()[1]

    def get_session_time(self):
        """Returns current time in the simulation"""
        return self.motor.current_time

    def __get_current_t(self, temperature_type):
        """Return the right temperature depending on *temperature_type*:
        'global', 'left' or 'right'"""
        temp_l, _, temp_r, _ = self.motor.get_stats()
        if temperature_type == 'left':
            current_temperature = temp_l
        elif temperature_type == 'right':
            current_temperature = temp_r
        else:
            current_temperature = temp_r + temp_l
        return current_temperature

    def run_until_t(self, target_temperature, temperature_type='global'):
        """Runs until the simulation reach the *target_temperature*.
        *temperature_type* could be 'global', 'left' or 'right' """
        current_temperature = self.__get_current_t(temperature_type)
        delta_temp = target_temperature - current_temperature
        if delta_temp < 0:
            compare_f = compare_t_down
            ratio = 0.9
        else:
            compare_f = compare_t_up
            ratio = 1.1
        while compare_f(target_temperature, current_temperature):
            self.motor.modify_speed(ratio)
            self.run(self.RUN_T)
            current_temperature = self.__get_current_t(temperature_type)

    def run_until_equilibrium(self, temperature_type='global', delta_t=None):
        """Runs until T is stable.
        *temperature_type* could be 'global', 'left' or 'right'
        *delta_t* overrides the default DELTA_T"""
        if delta_t:
            delta_t = delta_t
        else:
            delta_t = self.DELTA_T
        current_temperature = self.__get_current_t(temperature_type)
        self.run(self.RUN_STABLE)
        new_temperature = self.__get_current_t(temperature_type)
        while abs(current_temperature - new_temperature) > delta_t:
            current_temperature = new_temperature
            self.run(self.RUN_STABLE)
            new_temperature = self.__get_current_t(temperature_type)

    def remove_wall(self):
        """Removes the middle wall. The effect is not immediate (the wall will
        be removed when pressure = 0, in order to conserve the energy)"""
        self.motor.remove_wall()

    def reset_statistics(self):
        """Resets the MD simulation statistics"""
        self.motor.reset_stats()

    def change_hole(self, hole_height):
        """Changes the height of the hole"""
        self.motor.change_hole(hole_height)

if __name__ == '__main__':
    SESSION = MDSimulationSession("../simulations/ideal_gas")
