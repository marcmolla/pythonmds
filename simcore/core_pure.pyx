# cython: profile=True
"""Pure Cython Module"""
from __future__ import division
import cython
from libc.stdlib cimport malloc, free
from libc.math cimport floor, ceil, sqrt

#Distance of potential cutoff
cdef int CUTOFF = 3
#pre-calculated square of the cutoff distance
cdef int CUTOFF2 = CUTOFF **2
#Potential energy at cutoff distance
cdef double U_CUTOFF = 4*(1/CUTOFF**12-1/CUTOFF**6)

#Neighbour coordinates for the cell linked-list algorithm
# (0,0), (1,0), (1,1), (0,1), (-1,1)
cdef int n_pos_x[5]
n_pos_x[0] = 0
n_pos_x[1] = 1
n_pos_x[2] = 1
n_pos_x[3] = 0
n_pos_x[4] = -1
cdef int n_pos_y[5]
n_pos_y[0] = 0
n_pos_y[1] = 0
n_pos_y[2] = 1
n_pos_y[3] = 1
n_pos_y[4] = 1
cdef int n_neigbours = 5

cdef class MDSimulation(object):
    """Cython class for the simulation"""

    #Fixed dt value
    DELTA_T = 1/100
    #Steps per frame
    STEPS_PER_FRAME = 25
    #Space between molecules in the initial configuration
    PADDING = .6
    
    #Classes of internal wall
    NONE = 0
    FIXED = 1
    EFFUSION = 2
    #Not supported yet!
    FLOATING = 3
    
    #cython attributes of the class
    cdef double energy_p_l, energy_p_r
    cdef double energy_k_l, energy_k_r
    cdef double instant_pressure_l, instant_pressure_r
    cdef int n_left, n_right
    cdef public double wall_x
    cdef public int number_of_molecules, width, height
    cdef double *pos_x
    cdef double *pos_y
    cdef double *vel_x
    cdef double *vel_y
    cdef double *acc_x
    cdef double *acc_y
    cdef public double current_time
    cdef double stats_time, acum_t_l, acum_t_r, average_t_l, average_t_r
    cdef double acum_p_l, acum_p_r, average_p_l, average_p_r
    cdef int n_measures
    cdef bint remove_wall_flag
    cdef public double wall_hole_y_min, wall_hole_y_max
    
    def __init__(self, number_of_molecules, width = 60, height = 60,
                 wall_type = 0, wall_pos = None, wall_hole = None):
        """Creates the core of MD simulation.
        
        Arguments
        number_of_molecules -- Number of molecules of the simulation.
        width -- Width of the box in natural units. Default: 60
        height -- Height of the box in natural units. Default: 60
        wall_type -- One of NONE, FIXED, EFFUSION or FLOATING
        wall_pos -- x coordinate of the wall in natural units
        wall_hoe -- Width of the hole (only if wall_type is EFFUSION
        """
        #Potential and kinetic energy
        self.energy_p_l = 0
        self.energy_p_r = 0
        self.energy_k_l = 0
        self.energy_k_r = 0
        #Number of molecules
        self.number_of_molecules = number_of_molecules
        self.n_left = 0
        self.n_right = 0
        #Size of the box
        self.width = width
        self.height = height        
        #Create vectors
        self.pos_x = <double *>malloc(number_of_molecules * sizeof(double))
        self.pos_y = <double *>malloc(number_of_molecules * sizeof(double))
        self.vel_x = <double *>malloc(number_of_molecules * sizeof(double))
        self.vel_y = <double *>malloc(number_of_molecules * sizeof(double))
        self.acc_x = <double *>malloc(number_of_molecules * sizeof(double))
        self.acc_y = <double *>malloc(number_of_molecules * sizeof(double))
        #Time
        self.current_time = 0
        #Statistics
        self.stats_time = 0
        self.n_measures = 0
        self.average_t_l = 0
        self.acum_t_l = 0
        self.average_t_r = 0
        self.acum_t_r = 0
        #Wall creation
        if wall_type > self.NONE:
            if wall_pos != None and wall_pos < width:
                self.wall_x = wall_pos
            else:
                #if wall_pos is not indicate we put the wall in the middle
                self.wall_x = self.width/2
        else:
            #No wall
            self.wall_x = -1
        
        #Initial set-up of the molecules
        self.__set_molecules(self.PADDING)
        
        #Flag to remove the wall
        self.remove_wall_flag = False
        
        #Wall Hole
        if wall_hole:
            self.wall_hole_y_max = height/2 + wall_hole/2
            self.wall_hole_y_min = height/2 - wall_hole/2
        else:
            self.wall_hole_y_min = -1
            self.wall_hole_y_max = width + 1


    def __set_molecules(self, padding):
        """Initial position of the molecules
        
        The molecules are set sequentially in line, separated by *padding* 
        space. When the line reaches a wall, a new molecules line is set.
        Example using one space as padding:
         _________
        |         |
        |         |
        | x x     |
        | x x x x |
        |_________|
        
        Note that it is required at least two lines for a good simulation. One
        line produces 1D simulation.
        """
        cdef double x = padding
        cdef double y = padding
        cdef double radius = .5
        cdef int n
        cdef double real_width = self.width
        if self.wall_x > 0:
            real_width = self.wall_x
        for n in range(self.number_of_molecules):
            x += radius
            self.pos_x[n] = x
            self.pos_y[n] = y
            #reset vel and acc
            self.vel_x[n] = 0
            self.vel_y[n] = 0
            self.acc_x[n] = 0
            self.acc_y[n] = 0
            
            x += radius + padding
            if x > real_width - radius - padding:
                x = padding
                y += radius + padding
                if y > self.height - radius - padding:
                    #No more space
                    self.number_of_molecules = n
                    break

    def get_position(self, int molecule):
        """Returns the position (x,y) of a molecule"""
        return self.pos_x[molecule], self.pos_y[molecule]

    def get_total_energy(self):
        """Returns the Energy of the simulation"""
        return self.get_potential_energy()+self.get_kinetic_energy()

    def get_potential_energy(self):
        """Returns the potential energy of the simulation"""
        return self.energy_p_l + self.energy_p_r

    def get_kinetic_energy(self):
        """Returns the kinetic energy of the simulation"""
        return self.energy_k_l + self.energy_k_r

    def get_number_molecules(self):
        """Returns the number of molecules in each side"""
        return self.n_left, self.n_right
    
    def get_volumes(self):
        """Returns the volume of each side of the box"""
        if self.wall_x > 0:
            return self.height*self.wall_x, self.height*(self.width - self.wall_x)
        else:
            return self.height*self.width, 0
    
    def get_speed(self, int molecule):
        """Returns the speed (|v|) of a molecule"""
        return sqrt(self.vel_x[molecule]**2 + self.vel_y[molecule]**2)

    def velocity_verlet(self):  
        """Calculates a step (delta_t) using the velocity verlet algorithm"""
        cdef double delta_t = self.DELTA_T
        cdef double dt2 = delta_t*delta_t
        #pos = velocity * delta_t + 1/2*a*delta_t**2
        cdef int n
        for n in range(self.number_of_molecules):
            self.pos_x[n] += self.vel_x[n]*delta_t+.5*self.acc_x[n]*dt2
            self.pos_y[n] += self.vel_y[n]*delta_t+.5*self.acc_y[n]*dt2
            #First step of velocity using current acceleration
            self.vel_x[n] += 0.5*(self.acc_x[n])*delta_t
            self.vel_y[n] += 0.5*(self.acc_y[n])*delta_t
            #Detects in which part is the molecule
            if self.wall_x > 0:
                if self.pos_x[n] < self.wall_x:
                    self.n_left += 1
                else:
                    self.n_right += 1
            else:
                self.n_left += 1
        #Checks the boundaries of the box
        self.check_boundaries()
        #Update accelerations with Lennard-Jones potential
        #Always uses Cell List algorithm
        self.update_accelerations_cell_list()
        
        #Second step of velocity using updated acceleration
        for n in range(self.number_of_molecules):
            self.vel_x[n] += 0.5*(self.acc_x[n])*delta_t
            self.vel_y[n] += 0.5*(self.acc_y[n])*delta_t

    def step(self):
        """Do a step of the simulation of length delta_t"""
        #Reset Energy
        self.energy_p_l = 0
        self.energy_p_r = 0
        self.energy_k_l = 0
        self.energy_k_r = 0
        self.current_time += self.DELTA_T
        self.instant_pressure_l = 0
        self.instant_pressure_r = 0
        #Reset number of molecules
        self.n_left = 0
        self.n_right = 0
        #Do Velocity Verlet
        self.velocity_verlet()
        #Update kinetic energy
        self.update_kinetic_E()
        #Update statistics
        self.update_stats()

    def update_stats(self):
        """Update the simulation stats"""
        if self.n_left > 0:
            #Equipartition Theorem: T = <K>/N 
            self.acum_t_l += self.energy_k_l/self.n_left
        if self.n_right > 0:
            self.acum_t_r += self.energy_k_r/self.n_right
        self.n_measures += 1
        self.average_t_l = self.acum_t_l / self.n_measures
        self.average_t_r = self.acum_t_r / self.n_measures
        self.acum_p_l += self.instant_pressure_l
        self.acum_p_r += self.instant_pressure_r
        self.average_p_l = self.acum_p_l / self.n_measures
        self.average_p_r = self.acum_p_r / self.n_measures

    def reset_stats(self):
        """Reset counters stats"""
        self.stats_time = self.current_time
        self.n_measures = 0
        self.acum_t_l = 0
        self.acum_p_l = 0
        self.acum_t_r = 0
        self.acum_p_r = 0

    def get_stats(self):
        """Returns current statistics"""
        return self.average_t_l, self.average_p_l, self.average_t_r, self.average_p_r
    
    @cython.cdivision(True)
    def update_kinetic_E(self):
        """Calculates the kinetic Energy of the simulation"""
        cdef int n
        for n in range(self.number_of_molecules):
            if self.wall_x > 0:
                if self.pos_x[n] < self.wall_x:
                    # K = 0.5(v_x^2 + v_y^2)
                    self.energy_k_l += .5*(self.vel_x[n]**2 + self.vel_y[n]**2)
                else:
                    self.energy_k_r += .5*(self.vel_x[n]**2 + self.vel_y[n]**2)
            else:
                self.energy_k_l += .5*(self.vel_x[n]**2 + self.vel_y[n]**2)
    
    @cython.cdivision(True)      
    def check_boundaries(self):
        """Calculates the interaction between molecules and walls"""
        #Check boundaries
        cdef int n    
        for n in range(self.number_of_molecules):
            if self.pos_x[n] < 0.5:
                #F = -kx
                self.acc_x[n] = 50 * (0.5 - self.pos_x[n])
                #U = 0.5*k*x^2
                self.energy_p_l += 25 * (0.5 - self.pos_x[n])**2
                #Acc is positive
                self.instant_pressure_l += self.acc_x[n]
            elif self.pos_x[n] > self.width - 0.5:
                self.acc_x[n] = 50 * (self.width - 0.5 - self.pos_x[n])
                if self.wall_x > 0:
                    self.energy_p_r += 25 * (self.width - 0.5 - self.pos_x[n])**2
                    #Acc is negative
                    self.instant_pressure_r -= self.acc_x[n]
                else:
                    self.energy_p_l += 25 * (self.width - 0.5 - self.pos_x[n])**2
                    #Acc is negative
                    self.instant_pressure_l -= self.acc_x[n]
            else:
                #reset acc
                self.acc_x[n] = 0
            
            if self.pos_y[n] < 0.5:
                self.acc_y[n] = 50 * (0.5 - self.pos_y[n])
                if self.wall_x > 0:
                    if self.pos_x[n] < self.wall_x:
                        self.energy_p_l += 25 * (0.5 - self.pos_y[n])**2
                        self.instant_pressure_l += self.acc_y[n]
                    else:
                        self.energy_p_r += 25 * (0.5 - self.pos_y[n])**2
                        self.instant_pressure_r += self.acc_y[n]
                else:
                    self.energy_p_l += 25 * (0.5 - self.pos_y[n])**2
                    self.instant_pressure_l += self.acc_y[n]
            elif self.pos_y[n] > self.height - 0.5:
                self.acc_y[n] = 50 * (self.height - 0.5 - self.pos_y[n])
                if self.wall_x > 0:
                    if self.pos_x[n] < self.wall_x:
                        self.energy_p_l += 25 * (self.height - 0.5 - self.pos_y[n])**2
                        self.instant_pressure_l -= self.acc_y[n]
                    else:
                        self.energy_p_r += 25 * (self.height - 0.5 - self.pos_y[n])**2
                        self.instant_pressure_r -= self.acc_y[n]
                else:
                    self.energy_p_l += 25 * (self.height - 0.5 - self.pos_y[n])**2
                    self.instant_pressure_l -= self.acc_y[n]
            else:
                self.acc_y[n] = 0
            
            #Wall (Fixed). We use elastic potential energy
            if self.wall_x > 0 and self.wall_hole_y_min < 0:
                if self.pos_x[n] < self.wall_x + .5 and \
                self.pos_x[n] > self.wall_x:
                    #k=400
                    self.acc_x[n] = 800 * (self.wall_x + .5 - self.pos_x[n])
                    self.energy_p_r += 400 * (self.wall_x + .5 - self.pos_x[n])**2
                    #Acc is positive
                    self.instant_pressure_r += self.acc_x[n]
                elif self.pos_x[n] > self.wall_x - .5 and \
                self.pos_x[n] < self.wall_x:
                    self.acc_x[n] = 800 * (self.wall_x - .5 - self.pos_x[n])
                    self.energy_p_l= 400 * (self.wall_x - .5 - self.pos_x[n])**2
                    #Acc is negative
                    self.instant_pressure_l -= self.acc_x[n]
             
            #Wall (Effusion). We use hard wall
            if self.wall_x > 0 and self.wall_hole_y_min > -1:
                if self.pos_x[n] < self.wall_x + .5 and \
                self.pos_x[n] > self.wall_x and \
                (self.pos_y[n] >= self.wall_hole_y_max or \
                 self.pos_y[n] < self.wall_hole_y_min):
                    if self.vel_x[n] < 0:
                        self.vel_x[n] = -self.vel_x[n]
                        #Pressure is not calculated!!
                if self.pos_x[n] > self.wall_x - .5 and \
                self.pos_x[n] < self.wall_x and \
                (self.pos_y[n] >= self.wall_hole_y_max or \
                 self.pos_y[n] < self.wall_hole_y_min):
                    if self.vel_x[n] > 0:
                        self.vel_x[n] = -self.vel_x[n]

            #Check if we can remove the wall
            if self.wall_x > 0:
                #Removes the wall only if there are not interactions
                if self.remove_wall_flag == True and \
                self.instant_pressure_r == 0 and \
                self.instant_pressure_l == 0:
                    self.remove_wall_flag = False
                    self.wall_x = -1
                    self.reset_stats()
        
        #Calculates instant pressure
        if self.wall_x > 0:
            self.instant_pressure_l = self.instant_pressure_l /(2*self.wall_x+2*self.height)
            self.instant_pressure_r = self.instant_pressure_r /(2*(self.width-self.wall_x)+2*self.height)
        else:
            self.instant_pressure_l = self.instant_pressure_l /(2*self.width+2*self.height)

    @cython.cdivision(True)
    def update_accelerations_cell_list(self):
        """Calculates the interaction of the molecules using cell linked-list
        algorithm."""
        #Build linked list
        cdef int ncells_x = int(ceil(self.width/CUTOFF))
        cdef int ncells_y = int(ceil(self.height/CUTOFF))
        cdef double cell_w_x = self.width/ncells_x
        cdef double cell_w_y = self.height/ncells_y
        cdef int i, cell_x, cell_y,
        cdef int *cell_list = <int *>malloc(ncells_x*ncells_y * sizeof(int))
        cdef int *cell_linked_list = <int *>malloc(self.number_of_molecules * sizeof(int))
    
        #Initializes the cell list
        for i in range(ncells_x*ncells_y):
            cell_list[i] = -1
            
        #Builds the linked list
        for i in range(self.number_of_molecules):
            cell_y = int(floor(self.pos_y[i]/cell_w_y))
            cell_x = int(floor(self.pos_x[i]/cell_w_x))
            
            if cell_y < 0:
                cell_y = 0
            elif cell_y >= ncells_y:
                cell_y = ncells_y - 1
            if cell_x < 0:
                cell_x = 0
            elif cell_x >= ncells_x:
                cell_x = ncells_x - 1
                
            cell_linked_list[i] = cell_list[cell_x + cell_y*ncells_x]
            cell_list[cell_x + cell_y*ncells_x] = i

        cdef double cdx, cdy, cdx2, cdy2, cdistance2, cinverse2, csix, cdozen, cE_pot, cF
        cdef int m, other
    
        #Check the interaction of the cells
        for cell_x in range(ncells_x):
            for cell_y in range(ncells_y):
                n = cell_list[cell_x + cell_y*ncells_x]
                while n > -1:    
                    for other in range(n_neigbours):
                        m=-1
                        if other == 0:
                            m = cell_linked_list[n]
                        elif ((cell_x + n_pos_x[other] < ncells_x) and
                              (cell_x + n_pos_x[other] > -1) and
                              (cell_y + n_pos_y[other] < ncells_y)):
                            m = cell_list[cell_x + n_pos_x[other] + (cell_y + n_pos_y[other])*ncells_x]
                        while m > -1:
                            cdx = self.pos_x[m]-self.pos_x[n]
                            cdy = self.pos_y[m]-self.pos_y[n]
                            cdx2 = cdx*cdx
                            cdy2 = cdy*cdy
                            cdistance2 = cdx2+cdy2
                            if cdistance2 < CUTOFF2:
                                #Interaction!
                                #calculates the Lennard-Jones potential
                                cinverse2 = 1/cdistance2
                                csix = cinverse2*cinverse2*cinverse2
                                cdozen = csix*csix
                                #U = 4*(1/r^12-1/r^6) - U_{cutoff}
                                cE_pot= 4*(cdozen-csix) - U_CUTOFF
                                #F = 4*(12/r^13-6/r^7)
                                cF = 24*(2*cdozen-csix)*cinverse2
                                # acc_x = F * d_x/r
                                self.acc_x[m] += cF * cdx
                                self.acc_y[m] += cF * cdy
                                #3rd Newton Law!
                                self.acc_x[n] -= cF * cdx
                                self.acc_y[n] -= cF * cdy
                                if self.wall_x>0:
                                    if self.pos_x[n] > self.wall_x:
                                        self.energy_p_r += cE_pot
                                    else:
                                        self.energy_p_l += cE_pot
                                else:
                                    self.energy_p_l += cE_pot
                                
                            m = cell_linked_list[m]
                    n = cell_linked_list[n]
        free(cell_list)
        free(cell_linked_list)

    @cython.cdivision(True)         
    def update_accelerations(self):
        """Calculates the interactions of ALL molecules (NOT USED)"""
        cdef int n,m
        cdef double dx, dy, dx2, dy2, distance2, inverse2, six, dozen, E_pot, F
        for n in range(self.number_of_molecules):
            for m in range(n):
                dx = self.pos_x[m]-self.pos_x[n]
                dy = self.pos_y[m]-self.pos_y[n]
                dx2 = dx*dx
                dy2 = dy*dy
                distance2 = dx2+dy2
                if distance2 < CUTOFF2:
                    inverse2 = 1/distance2
                    six = inverse2*inverse2*inverse2
                    dozen = six*six
                    E_pot= 4*(dozen-six) - U_CUTOFF
                    F = 24*(2*dozen-six)*inverse2
                    self.acc_x[m] += F * dx
                    self.acc_y[m] += F * dy
                    self.acc_x[n] -= F * dx
                    self.acc_y[n] -= F * dy

                    self.energy_p += E_pot

    def modify_speed(self, factor):
        """Modifies the molecules speed by a *factor"""
        cdef int n
        for n in range(self.number_of_molecules):
            self.vel_x[n] *= factor
            self.vel_y[n] *= factor
        self.reset_stats()

    def remove_wall(self):
        """Remove the wall (if exists). In order to avoid modifications in the
        total energy, the wall will be removed when there are no interactions
        with molecules"""
        self.remove_wall_flag = True

    def change_hole(self, wall_hole):
        """Change the size of the hole"""
        self.wall_hole_y_max = self.height/2 + wall_hole/2
        self.wall_hole_y_min = self.height/2 - wall_hole/2

