#from distutils.extension import Extension
from distutils.core import setup
from Cython.Build import cythonize

#from Cython.Compiler.Options import directive_defaults
#directive_defaults['linetrace'] = True
#directive_defaults['binding'] = True

#extensions = [
#   Extension("core_c", ["core_c.pyx"], define_macros=[('CYTHON_TRACE', '1')])
#]


setup(
#    ext_modules = cythonize(extensions)
    ext_modules = cythonize("simcore/*.pyx")
)
